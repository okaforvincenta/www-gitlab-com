---
layout: handbook-page-toc
title: "SOX Internal Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


### 1. Risk Assessment and Scoping

### 2. Sarbanes-Oxley(SOX) Management Control Assessment Plan

### 3. [Entity Level Controls](/handbook/finance/sox-internal-controls/entity-level-controls/)

### 4. [IT General Controls](/handbook/engineering/security/security-assurance/security-compliance/ITGC/)

### 5. Business Process Controls

Business Process controls are the internal controls that are designed to provide reasonable assurance on business operations, process, efficiency & effectiveness. 


1. [Quote to Cash](/handbook/finance/sox-internal-controls/quote-to-cash/) 
1. [Procure to Pay](/handbook/finance/sox-internal-controls/procure-to-pay/)
1. [Hire to Retire](/handbook/finance/sox-internal-controls/hire-to-retire/)
1. [Regulatory](/handbook/finance/sox-internal-controls/regulatory/)
1. [Record to Report](/handbook/finance/sox-internal-controls/record-to-report/)
1. [Stock Compensation](/handbook/finance/sox-internal-controls/stock-compensation/)

### 6. [SOX Certifications](/handbook/finance/sox-internal-controls/sox-certifications/)
