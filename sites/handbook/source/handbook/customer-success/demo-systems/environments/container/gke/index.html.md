---
layout: handbook-page-toc
title: "GKE Container Sandbox"
description: "The GitLab Demo Systems GKE container environment handbook pages provides an overview of how our GCP cluster infrastructure is configured and has answers to frequently asked questions."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview 

We have a GCP project (`group-cs`) for all Customer Success team members that is part of the GitLab consolidated billing organization.

The Demo Systems team is responsible for provisioning GKE clusters for each user to provide you administrative access to a cluster for demo purposes, or adding as a group-level cluster with the Demo Cloud Omnibus instance.

We are iterating on the cluster management methodology. In the mean time, you can do whatever you need to with your cluster.

For provisioning requests and support, please ask in `#demo-systems`.
