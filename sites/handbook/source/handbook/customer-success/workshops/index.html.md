---
layout: handbook-page-toc
title: Customer Workshops
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Available Workshops

2021-01 [Skills Exchange Enablement on Customer Workshops](https://www.youtube.com/watch?v=kSbTZgPLKUE&feature=youtu.be) (internal GitLab link)

TAM-Created, Enablement Focus:

### Stage-specific Workshops

- [CI Workshop](/handbook/customer-success/workshops/ci-workshop.html)
- [Secure Workshop](/handbook/customer-success/workshops/secure/)

### Cross-stage Workshops

WIP

