---
layout: handbook-page-toc
title: "Product Design Pairs - FY22-Q1"
description: "Product designer pairs rotation schedule"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Learn more about [Pair Designing](/handbook/engineering/ux/how-we-work/#pair-designing).

[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Product Designer   | Time Zone | Design Pair        |
|--------------------|-----------|--------------------|
| Amelia Bauerly          | GMT-8     | Taurie Davis            |
| Alexis Ginsberg         | GMT-6     | Jeremy Elder            |
| Annabel Gray             | GMT-6     | Austin Regnery          |
| Amanda Hughes           | GMT-6     | Pedro Moreira da Silva |
| Austin Regnery          | GMT-5     | Annabel Gray             |
| Andy Volpe            | GMT-5     | Mike Nichols          |
| Becka Lippert          | GMT-6     | Tim Noah             |
| Camellia Yang             | GMT+1     | Daniel Mora             |
| Daniel Fosco            | GMT+1     | Matej Latin            |
| Daniel Mora             | GMT+1     | Camellia Yang             |
| Emily Bauman           | GMT-5     | Nadia Sotnikova        |
| Emily Sybrant          | GMT-6     | Nick Brandt           |
| Holly Reynolds         | GMT-5     | Maria Vrachni          |
| Iain Camacho          | GMT+1     | Libor Vanc             |
| Jeremy Elder            | GMT-6     | Alexis Ginsberg         |
| Jeremy Ostrowski        | GMT-5     | Kevin Comoli           |
| Kevin Comoli           | GMT+1     | Jarek Ostrowski        |
| Libor Vanc             | GMT-8     | Iain Camacho           |
| Matej Latin            | GMT+2     | Daniel Fosco            |
| Michael Le               | GMT+11    | Veethika Mishra           |
| Matthew Nearents         | GMT-8     | Sunjung Park             |
| Mike Nichols          | GMT-5     | Andy Volpe            |
| Maria Vrachni          | GMT+0     | Holly Reynolds         |
| Nick Brandt           | GMT-7     | Emily Sybrant          |
| Nick Post             | GMT+0     | Rayana Verissimo        |
| Nadia Sotnikova        | GMT+7/+8     | Emily Bauman           |
| Pedro Moreira da Silva | GMT+1     | Amanda Hughes           |
| Rayana Verissimo        | GMT+1     | Nick Post             |
| Sunjung Park             | GMT+1     | Matthew Nearents         |
| Tim Noah             | GMT+0     | Becka Lippert          |
| Veethika Mishra           | GMT+5.5   | Michael Le               |
