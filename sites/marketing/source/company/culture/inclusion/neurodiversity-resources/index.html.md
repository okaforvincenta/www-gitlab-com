---
layout: markdown_page
title: "Neurodiversity Resources"
description: "This page provides resources for team members, managers and those who identify as neurodiverse"
canonical_path: "/company/culture/inclusion/neurodiversity-resources/"
---

## The purpose of Neurodiversity Resources 

- Provide resources for those who identify as neurodiverse, to help them navigate remote work, management difficulties and other situations and challenges that they may face at work. 
- Provide team members and managers+ resources to help provide a better team member experience for there colleagues and direct reports who identify as neurodiverse. 

## Resources for those who identify at Neurodiverse
- [Neurodiverse is a competitive advantage](https://hbr.org/2017/05/neurodiversity-as-a-competitive-advantage)
- [ADHD in the workplace](https://www.webmd.com/add-adhd/adhd-in-the-workplace)

## Resources for Team Members and Managers+

- [How to embed neurodiversity into your people management practices](https://www.hrzone.com/perform/people/how-to-embed-neurodiversity-into-your-people-management-practices)
- [Austistic Advocacy Reources](https://autisticadvocacy.org/resources/accessibility/)
- [Certified Neurodiverse Workplace](https://ibcces.org/certified-neurodiverse-workplace/)
- [Understanding the benefits of neurodiversity in the workplace](https://www.hays.com.au/blog/insights/understanding-the-benefits-of-neurodiversity-in-the-workplace)
- [Neurodiversity Resources for Employers](https://www.neurodiversityhub.org/resources-for-employers)
